# SPISensorBMP280 and  FW15Protocol

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
<<<<<<< HEAD
git remote add origin https://gitlab.com/sebasms1.99/spisensorbmp280.git
=======
git remote add origin https://gitlab.com/sebasms1.99/FW15Protocol.git
>>>>>>> c6659f2d036993d9cb63092acc5c00f83586ab6d
git branch -M main
git push -uf origin main
```

## Integrate with your tools


- [ ] [Set up project integrations](https://gitlab.com/sebasms1.99/FW15Protocol/-/settings/integrations)


## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
SPI Sensor BMP280 & Protocol Commands
## Description
In this repository you will find the work of FW2.

In which I make the following points:

Set the sensor to continuously read temperature and pressure.

Create a routine to read the "Press" records.

Create a routine to get the raw pressure value by bit shifting.

Implement the raw temperature to pascal conversion formula according to Section 3.12. Calculation of pressure and temperature.

Read the temperature and pressure periodically every second.

Making use of an STM32F429ZI and a BMP280 sensor, which communicate by SPI

In this repository the comic was implemented by specific commands, for this 4 libraries are used, which are:

uart_driver in charge of communication between devices.

ring_buffer embedded in packing the data into a ring buffer

protocol in charge of carrying out the protocols for each command.

command_parser in charge of carrying out the actions according to the command.

Commands and actions:


*GD#      ->      Sends the state of the doors (Opened/Closed) to the host
*GF#      ->      Sends the FW version (Updated every time a release is made) to the host.
*GH#      ->      Send the state of the heater, indicating if it's turned on or off.
*GS#      ->      Calculates the percentage of the fan based on the duty cycle and sends it to the host
*GT#      ->      Send the last sample of temperature to the host
*GU#      ->      Sends the total time the unit has been running.
*SD0#     ->      Set the state of the door to closed
*SD1#     ->      Set the state of the door to opened
*SSx#     ->      Changes the duty cycle of the PWM to different values: 0%, 25%, 50%, 75%, 100%
*NACK#    ->      Command not defined



NOTE:
An extra command *GL# is added which allows sending a data packet to the ESP32 so that it can be parsed and sent to the topic.

Using the blue button of the ETM32 the HEATER and FUN are turned on or off, and the port is opened or closed. To read the speed of the FUN, the FUN pin (PG13) must be connected to the adc (PA1).


## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
