#ifndef __PROTOCOL_H_
#define __PROTOCOL_H_

#include <stdint.h>

#define PROTOCOL_PREAMBLE '*'
#define PROTOCOL_POSAMBLE '#'

typedef enum protocol_operation_ {
	OPERATION_GET = 'G',
	OPERATION_SET = 'S',

}protocol_operation_t;

typedef enum protocol_element_t {
	ELEMENT_LED = 'L',//Get all data
	ELEMENT_TEMPERATURE = 'T',
	ELEMENT_DUTYCYCLE = 'S',
	ELEMENT_DOOR = 'D',
	ELEMENT_FW = 'F',
	ELEMENT_UNIT = 'U',
	ELEMENT_HEATER= 'H',


}protocol_element_t;

typedef struct rx_packet_ {
	uint8_t operation; /* !< Might be a set or get operation > */
	uint8_t element;
	uint8_t value;

}__attribute__((__packed__)) rx_packet_t;

#endif /* __PROTOCOL_H_ */
