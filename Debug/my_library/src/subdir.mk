################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../my_library/src/command_parser.c \
../my_library/src/protocol.c \
../my_library/src/ring_buffer.c \
../my_library/src/uart_driver.c 

OBJS += \
./my_library/src/command_parser.o \
./my_library/src/protocol.o \
./my_library/src/ring_buffer.o \
./my_library/src/uart_driver.o 

C_DEPS += \
./my_library/src/command_parser.d \
./my_library/src/protocol.d \
./my_library/src/ring_buffer.d \
./my_library/src/uart_driver.d 


# Each subdirectory must supply rules for building sources it contributes
my_library/src/%.o my_library/src/%.su my_library/src/%.cyclo: ../my_library/src/%.c my_library/src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F429xx -c -I../Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I"D:/Titoma/Firmware/FW2/Sensor/SPI/my_library/inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-my_library-2f-src

clean-my_library-2f-src:
	-$(RM) ./my_library/src/command_parser.cyclo ./my_library/src/command_parser.d ./my_library/src/command_parser.o ./my_library/src/command_parser.su ./my_library/src/protocol.cyclo ./my_library/src/protocol.d ./my_library/src/protocol.o ./my_library/src/protocol.su ./my_library/src/ring_buffer.cyclo ./my_library/src/ring_buffer.d ./my_library/src/ring_buffer.o ./my_library/src/ring_buffer.su ./my_library/src/uart_driver.cyclo ./my_library/src/uart_driver.d ./my_library/src/uart_driver.o ./my_library/src/uart_driver.su

.PHONY: clean-my_library-2f-src

